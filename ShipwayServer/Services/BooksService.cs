using ShipwayServer.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ShipwayServer.Services;

public class BooksService {
    private readonly IMongoCollection<Book> _booksCollection;

    public BooksService(IOptions<ShipwayDatabaseSettings> shipwayDatabaseSettings) {

        var mongoClient = new MongoClient(shipwayDatabaseSettings.Value.ConnectionString);
        var mongoDatabase =  mongoClient.GetDatabase(shipwayDatabaseSettings.Value.DatabaseName);
        _booksCollection = mongoDatabase.GetCollection<Book> (shipwayDatabaseSettings.Value.BooksCollectionName);
    }

    public async Task<List<Book>> GetAsync() =>
        await _booksCollection.Find(_ => true).ToListAsync();
    public async Task<Book?> GetAsync(string id) =>
        await _booksCollection.Find(x => x.Id == id).FirstOrDefaultAsync();
    public async Task CreateAsync(Book newBook) => 
        await _booksCollection.InsertOneAsync(newBook);
    public async Task UpdateAsync(string id, Book updateBook) =>
        await _booksCollection.ReplaceOneAsync(x => x.Id == id, updateBook);
    public async Task DeleteAsync(string id) =>
        await _booksCollection.DeleteOneAsync(x => x.Id == id);
    
}